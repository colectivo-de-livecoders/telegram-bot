# frozen_string_literal: true

require "telegram/bot"

token = ENV["BOT_TOKEN"]

Telegram::Bot::Client.run(token) do |bot|
  bot.listen do |message|
    begin
      if message.new_chat_members && message.new_chat_members != []
        bot.api.send_message(
          chat_id: message.chat.id,
          text:    "Bienvenide, #{message.from.first_name} o/. " \
                   "Te invitamos a leer el codigo de convivencia: " \
                   "https://colectivo-de-livecoders.gitlab.io/#coc"
        )
      end

      case message.text
      when /:[c(]/i
        bot.api.send_message(
          chat_id: message.chat.id,
          text: "te mando un abrazo :#{['c', '('].sample}"
        )
      when /derby/i
        bot.api.send_message(
          chat_id: message.chat.id,
          text:    "aww <3"
        )
      when /open ?source/i
        bot.api.send_message(
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id,
          text:    "no querrás decir software libre?"
        )
      when /ha(y|bria|bría) que/i
        bot.api.send_message(
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id,
          text:    "che que buena idea, por que no la haces?"
        )
      when /\bbugs?\b/i
        bot.api.send_message(
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id,
          text:    "patches welcome"
        )
      when /\bgat(it)?[oia]s?\b/i
        unless /\bmacri\b/i =~ message.text
          bot.api.send_message(
            chat_id: message.chat.id,
            text:    "LXS GATITXS SON LO MEJOR"
          )
        end
      when /\\o(\Z|\s)|(\A|\s)o\//i
        bot.api.send_message(
            chat_id: message.chat.id,
            text:    "o/"
          )
      when /\bbot\b/i
        bot.api.send_message(
            chat_id: message.chat.id,
            text:    ["a quien le habla?", "que ondi, hay un bot por acá? :O", "no querras decir cyborg?"].sample
          )
      when /windows/i
        bot.api.send_message(
            chat_id: message.chat.id,
            text:    "eso en GNU/Linux no pasa xD"
          )
      when /ruby/i
        bot.api.send_message(
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id,
          text:    "ruby love <3"
        )
      when /torrent/i
        bot.api.send_message(
          chat_id: message.chat.id,
          reply_to_message_id: message.message_id,
          text:    ["compartir es bueno", "copiar no es robar", "torrent o patria","si no torrenteamos, la cultura se netflixea"].sample
        )
      end
    rescue Telegram::Bot::Exceptions::ResponseError => e
      puts "ResponseError, #{e}"
    end
  end
rescue Faraday::ConnectionFailed => e
  # esto pasa porque fiber reinicia el router de vez en cuando (bastante seguido)
  sleep 200
  retry
end
